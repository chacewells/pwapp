const editController = (function () {
	const CRED_LIST_LOCATION = '../cred-list/cred-list.html';
	let credsDisplayElem;
	let editing = false;
	let id = null;
	let siteField, usernameField, passwordField;
	const credsService = new CredsService();
	
	window.addEventListener('load', () => {
		credsDisplayElem = document.getElementById('creds-display');
		initParams();
		initFields();
	});
	
	const module = {
		onSubmit: onSubmit,
		onCancel: onCancel
	};
	
	function initParams() {
		let params = new URLSearchParams(location.search);
		for (let param of params) {
			switch (param[0]) {
				case 'editing':
					editing = param[1] === 'true';
					break;
				case 'id':
					id = param[1];
					break;
				default:
					continue;
			}
		}
	}

	function initFields() {
		siteField = document.getElementById('site');
		usernameField = document.getElementById('username');
		passwordField = document.getElementById('password');

		if (id) {
			const creds = credsService.retrieveCreds();
			siteField.value = creds[id].site;
			usernameField.value = creds[id].username;
			passwordField.value = creds[id].password; // TODO decryption
		}
	}
	
	function onSubmit() {
		const record = {
			site: siteField.value,
			username: usernameField.value,
			password: passwordField.value
		};
		addToStorage(record);
	
		// displayCreds();
		navigateToCredList();
	}

	function onCancel() {
		navigateToCredList();
	}

	function navigateToCredList() {
		window.location = CRED_LIST_LOCATION;
	}
	
	function displayCreds() {
		const creds = credsService.retrieveCreds();
		credsDisplayElem.innerHTML = JSON.stringify(creds);
		credsDisplayElem.style.display = 'block';
	}
	
	function addToStorage(record) {
		const creds = credsService.retrieveCreds();
		if (editing)
			credsService.update(id, record);
		else
			credsService.add(record);
	}
	
	return module;
}());
