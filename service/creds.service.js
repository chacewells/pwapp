const CredsService = (function () {
	const CREDS_KEY = 'credentials';
		
	function storeCreds(creds) {
		const credsJson = JSON.stringify(creds);
		window.localStorage.setItem(CREDS_KEY, credsJson);
	}
	
	function insertOrdered(creds, record) {
		let i = 0;
		while (i < creds.length && record.site > creds[i].site)
			++i;
	
		creds.splice(i, 0, record);
	}
	
	function findAndReplace(creds, record) {
		let i = 0;
		for (; i < creds.length; ++i)
			if (creds[i].site === record.site)
				break;
	
		creds.splice(i, 1, record);
	}

	return class {
		retrieveCreds() {
			const credsJson = window.localStorage.getItem(CREDS_KEY);
			const creds = credsJson ? JSON.parse(credsJson) : [];
		
			return creds.slice();
		}
		
		add(record) {
			const creds = this.retrieveCreds();
			insertOrdered(creds, record);
			storeCreds(creds);
		}
	
		update(id, record) {
			const creds = this.retrieveCreds();
			creds[id] = record;
			storeCreds(creds);
		}
	};
}());
