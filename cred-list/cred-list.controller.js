const credsService = new CredsService();
const GLYPHICON_CLASS = 'glyphicon';
const GLYPHICON_EYE_OPEN_CLASS = 'glyphicon-eye-open';
const GLYPHICON_EYE_CLOSE_CLASS = 'glyphicon-eye-close'
const GLYPHICON_EDIT_CLASS = 'glyphicon-edit';
const EDIT_CREDENTIAL_CLASS = 'edit-credential';
const PASSWORD_LISTING_CLASS = 'password-listing';
const EDIT_PATH = '../edit/edit.html';

let pwListBody;

window.addEventListener('load', () => {
	pwListBody = document.getElementById('cred-list-body');
	fillCreds();
});

function fillCreds() {
	const creds = credsService.retrieveCreds();
	creds.forEach((cred, id) => {
		pwListBody.appendChild(
			credRow(id, cred.site, cred.username, cred.password));
	});
}

function credRow(id, site, username, password) {
	const tr = document.createElement('tr');
	tr.appendChild(editCredentialTd(id));
	tr.appendChild(td(site));
	tr.appendChild(td(username));
	tr.appendChild(pwTd(password));
	
	return tr;
}

function td(text) {
	const td = document.createElement('td');
	td.textContent = text;
	return td;
}

function pwTd(password) {
	const td = document.createElement('td');
	const inputElement = pwInput(password);
	td.appendChild(inputElement);
	const i = pwInputControl(inputElement);
	td.appendChild(i);

	return td;
}

function pwInput(password) {
	const input = document.createElement('input');
	input.type = 'password';
	input.value = password;
	input.readOnly = true;
	input.classList.add(PASSWORD_LISTING_CLASS);

	return input;
}

function pwInputControl(inputElement) {
	const i = document.createElement('i');
	i.classList.add(GLYPHICON_CLASS);
	i.classList.add(GLYPHICON_EYE_OPEN_CLASS);
	i.classList.add('pull-right');
	i.style = 'cursor: pointer';
	i.addEventListener('click', toggleInputHidingControl(inputElement));

	return i;
}

function toggleInputHidingControl(input) {
	return (event) => {				
		const control = event.target;
		if (input.type === 'text') {
			input.type = 'password';
			control.classList.remove(GLYPHICON_EYE_CLOSE_CLASS);
			control.classList.add(GLYPHICON_EYE_OPEN_CLASS);
		} else {
			input.type = 'text';
			control.classList.remove(GLYPHICON_EYE_OPEN_CLASS);
			control.classList.add(GLYPHICON_EYE_CLOSE_CLASS);
		}
	};
}

function editCredentialTd(id) {
	const td = document.createElement('td');
	const a = editCredentialControl(id);
	td.appendChild(a);

	return td;
}

function editCredentialControl(id) {
	const a = document.createElement('a');
	a.classList.add(GLYPHICON_CLASS);
	a.classList.add(GLYPHICON_EDIT_CLASS);
	a.href = `${EDIT_PATH}?id=${id}&editing=true`;

	return a;
}

function onAddNew() {
	window.location = EDIT_PATH;
}
